.. sectionauthor:: William Giokas <1007380@gmail.com>

The Linux File System
*********************

Partitioning
============

Partitioning a disk involves splitting a disk into one or more logical
storage units, commonly referred to as 'partitions.' When something is put
on a disk, the disk must first be partitioned. Unpartitioned disks are a
lot less useful than their partitioned counterparts. Luckily, creating
partitions is not a difficult task, and there are many tools out there
that make it very easy.

Partition tables
----------------

Partition tables define the beginning and end of a disk, as well as the
boot status of the partitions and their usage. There are two types of
partition tables: a Master Boot Record (MBR) and a GUID Partition Table. The
two are simply different ways of writing partition tables to disks.

Each partition of a disk is treated like a separate disk.

Master Boot Record
^^^^^^^^^^^^^^^^^^

The Master Boot Record was introduced in 1983 to support the, at that time
large, 10 MB hard disks of the IBM PC XT which still used the FAT12 file
system. It is still in use today, though is largely being replaced by the
GUID Partition Table. 

The MBR is a special boot sector set in the first 512 or more bytes of the
hard drive. It holds information on how the partitions are organized. The
disk described by an MBR can not exceed 2 terabytes. This has
become very limiting in modern times with disks commonly meeting and
exceeding 2 terabytes. It also limits the disk to just 4 partitions.

An MBR and GPT can coexist, but it is recommended to use a GPT.o

The ``fdisk`` utility can be used to partition drives using an MBR.

GUID Partition Table
^^^^^^^^^^^^^^^^^^^^

A GUID Partition Table (GPT) is the current standard partition table for
disks. It forms part of the EFI standard, and it can be used with disks as
large as 9.4 zettabytes, 9,400,000 petabytes, or 9,400,000,000 terabytes. 

It also allows for users to make up to 128 primary partitions with a single
GPT. Most modern Windows and Linux machines default to using this new GPT.

The ``gdisk`` utility can be used to partition drives using a GPT.

Why Partition?
--------------

There are many reasons to break a disk or any size into smaller, more
manageable pieces. 

Philosophical
^^^^^^^^^^^^^

Large drives can get extremely cluttered and hard to work with. With
multi-terabyte drives becoming more and more common, they can become
extremely cluttered. Creating smaller disks on the large disk can be
beneficial to this end, allowing you to treat different partitions as
physically separate disks. On solid state media, small file systems are
extremely efficient, as the seek times between partitions is reduced to
nanoseconds. Spinning disks with multiple small partitions is still better
than one large partition.

Technical
^^^^^^^^^

Partitioning allows you to limit the amount of disk space that certain
directories can hold and allows for quotas to be enforced by hardware rather
than software. 

Certain file systems, like ext4 and btrfs, support massive capacities,
whereas FAT12, cannot store very much data. 

It also allows for a single disk to contain multiple operating systems that
can be chosen at boot time, commonly referred to as 'multi-boot' systems.

You can also secure data much more easily using separate partitions. If one
partition is damaged by software, if that software did not have access to
the other partitions, then those partitions are untouched. Mounting
file systems as read-only, or 'ro,' can stop unwanted things from being
written to the disk.

Types of Partitions
-------------------

There are two main types of partitions that disks can have: data and swap.
As of Linux 2.6, swap files are just as fast as swap partitions, so it is
recommended to use those instead.

Naming
^^^^^^

Windows and Linux use drastically different file systems layouts. Windows
treats block devices specially, whereas everything in a Linux system is a
file (even the devices). 

When viewing partitions on a Windows machine, you will see letters such as
C, D or E. 

Linux stores your file system labels in a file system hierarchy located under
the /dev pseudo-file system. Hard drives can be identified by their name.

``/dev/sdxN`` are file systems on SATA, PATA, or SCSI devices. The ``x`` is
used to identify which drive it is, a being the first, b the second, and
so on. ``N`` is the partition number, ranging from 1 to 4 on non-LVM MBR
disks, or 1-128 on non-LVM GPT disks. ``/dev/hdxN`` is used for older IDE
based drives.

You can use the ``df`` utility to get details on the devices' partitions.

RAM disks are discouraged, and are no longer named ``/dev/ramNN``, but are
instead called simply ``tmpfs``

RAID arrays can have many names. The ones on the COD server use the naming
scheme ``/dev/cciss/cNdN[pN]``, the N's standing for the controller, disk
and partition respectively.

File System Types
^^^^^^^^^^^^^^^^^

Linux has one of the largest choices of file systems of any operating system,
mostly due to it being open source. Some common ones are:

* ext2, ext3, ext4

  The standard file systems for a Linux system, these extended file systems.
  ext4 is now the standard file system on any modern Linux system, though it
  is backwards and mostly forwards compatible with the previous iterations.
  These are all journaling file systems.

* reiserFS

  A journaling file system that is no longer maintained.

* btrfs

  A copy-on-write file system that is expected to supersede ext4 in the next
  few years. It includes support for snapshots, checksums, and pooling that
  is lacking on many Linux file systems.

* JFS

  A journaling file system created by IBM their family of operating systems.
  It is supported on Linux.

* XFS

  Another journaling file system created by Lilicon Graphics for their IRIX
  operating system. It has been ported tot he Linux kernel and is extremely
  efficient with parallel I/O.

Recommended Partitions
----------------------

It is recommended to have at least 2 file systems on any Linux machine:

* The root file system

  This contains the base of your computer. It is assigned the identifier
  ``/``, indicating that it is the very highest level on your system.

* The boot partition

  Certain bootloaders must load the boot partition separately from the rest
  of the system, making a partition with the identifier ``/boot`` a
  necessity on some systems.

Some other partitions that are common:

* ``/home``

  The base for user directories. If you have a ``/home`` partition, then
  your user and system data will remain separate.

* ``/var``

  ``/var`` is used for variable data like logs and mail spools. Putting this
  in a separate partition or on a separate disk can stop your disk from
  becoming too crowded with log files

* ``/tmp``
  
  Use of the ``/tmp`` partition is discouraged, in favor of using a
  temporary file system mounted to the system's RAM.

* ``/usr``
  
  Stores binaries accessed by the user or administrator.

.. warning::
  Booting with a separate ``/usr`` is broken without an initial RAM
  disk. Do not make a separate ``/usr`` unless you know what you are doing.

Getting Information on File Systems
-----------------------------------

There are many tools on Linux to get information on the file systems mounted
to your computer:

* The ``df`` utility

  The standard Unix disk usage utility outputs system information pertaining
  to the partitions and hard disks you have mounted. Here is some example
  output:

::

    Filesystem      Size  Used Avail Use% Mounted on
    rootfs          119G   49G   66G  43% /
    dev             3.9G     0  3.9G   0% /dev
    run             3.9G  504K  3.9G   1% /run
    /dev/sda1       119G   49G   66G  43% /
    tmpfs           3.9G   72K  3.9G   1% /dev/shm
    tmpfs           3.9G     0  3.9G   0% /sys/fs/cgroup
    tmpfs           3.9G  4.0K  3.9G   1% /tmp
    /dev/sda2       244M   45M  199M  19% /boot/efi
    
As you can see, there is one physical disk on this system that is mounted to
``/`` and ``/boot/efi``. There are three temporary file systems mounted to
``/tmp``, ``/sys/fs/cgroup``, and ``/dev/shm``. These are all stored in RAM
and are considered volatile. 

* The ``/etc/fstab`` file

  This file dictates how the file systems are mounted in a system.

* The ``/proc/partitions`` file

  Gives detailed information on the partitions connected to a computer.

* The ``/etc/mtab`` file

  Contains mount information for currently mounted devices.

* The ``/proc/mounts`` file

  Lists all file systems mounted to the computer, along with information
  about the partitions.


Virtual File Systems
====================

On Linux, accessing any file is the same no matter what kind of file system
the file is on or where it is located. Linux also uses pseudo-file systems
such as ``/proc`` and ``/sys`` for process and system information. These
behave as any other file system would. 

::
    
    ┌───────────────┐
    │  User Process │
    └──────┬────────┘
           │
           │ System call (trap)
    ┌──────┼──────────────────────────────────────────────────────────┐
    │   ┌──┴──────────────────────┐                                   │
    │   │ System Call Interface   │           Linux                   │
    │   └──────────┬──────────────┘             Kernel                │
    │              │                                                  │
    │         ┌────┴─────┐                                            │
    │         │   VFS    │                                            │
    │         └────┬─────┘                                            │
    │              │                                                  │
    │   ┌──────────┴──┬────────┬─────────┬───────┐                    │
    │   │             │        │         │       │                    │
    │ ┌─┴──────┐  ┌───┴──┐  ┌──┴────┐  ┌─┴─┐  ┌──┴──┐                 │
    │ │MINIX FS│  │DOS FS│  │ext4 FS│  │NFS│  │btrfs│                 │
    │ └─┬──────┘  └───┬──┘  └──┬────┘  └─┬─┘  └──┬──┘                 │
    │   │             │        │         │       │                    │
    │   └─────────┬───┴────────┴─────────┴───────┘                    │
    │             │                                                   │
    │       ┌─────┴──────────┐                                        │
    │       │ Buffer Cache   │                                        │
    │       └─────┬──────────┘                                        │
    │             │                                                   │
    │         ┌───┴────────────┐                                      │
    │         │ Device Drivers │                                      │
    │         └──────┬─────────┘                                      │
    └────────────────┼────────────────────────────────────────────────+
                     │
    ┌────────────────┼────────────────┐   * diagram taken from "Design and 
    │   ┌────────────┴───┐  Hardware  │     Implementation of the Second 
    │   │Disk Controller │            │     Extended Filesystem" presented
    │   └────────────────┘            │     at the First Dutch International
    └─────────────────────────────────┘     Symposium on Linux. Modified by
                                            William Giokas.


File System Corruption
======================

If the computer is not powered off gracefully (all file systems are not
synced and unmounted before the power is removed) then there is a
possibility of data loss. Running ``fsck`` on Linux will check your
file systems for consistency and will alert the user that there is
corruption, and sometimes even try to fix it. Journaling file systems help
to reduce the data loss on an ungraceful shutdown by writing changes first
to a journal entry, then committing them later. If the computer shuts down
before the file system can sync up the disks, then there is still a journal
entry that can help to restore that lost data on the next boot.

Journaling on file systems is not a requirement, but a feature.

File System Layout
==================

The Linux file system is broken into the following pieces::
    
    ┌────┬────┬────────┬────────────────────────────────────┐
    │ BB │ SB │   IL   │                 DB                 │
    └────┴────┴────────┴────────────────────────────────────┘
          BB: Boot Block    IL: inode List        
          SB: Super Block   DB: Data Blocks       From 'The Design of the 
                                                  UNIX Operating System', by
                                                  Maurice J. Bach.

UFS and later file systems have the following layout:

* boot block: Code to bootstrap the Operating System
* superblock: Contains information about the layout of blocks on the entire
  partition. Most file systems released after ext2 have multiple copies.
* inode table: List of inodes (information/index nodes)
* data blocks: Actual data on the hard drive in the directories.

Inodes
------

Inodes are information nodes, sometimes called index nodes. These keep
information about the location of the files and directories on the file
system. ``ls -i`` and ``stat`` show inode attributes for files, ``df -i``
shows information for device nodes.

There is a lot of information contained in an inode:

* link counter
* size
* user id
* access privileges
* timestamps
* data pointers, direct or indirect

Even files with several links will only have one inode number.

File System Hierarchy Standard
------------------------------

The typical UNIX/Linux file system hierarchy starts with the ``/``, or root
file system level. This is not to be confused with the ``/root`` directory
containing the root users account files.

The locations in the file system are determined by the type of file:

Shareable or Unshareable:

* sharable files: Files that are usable on multiple systems while only being
  stored on a single system.
* unsharable files: Files specific to the host that cannot be shared. These
  include system-specific files in the  ``/sys`` directory.

Variable or Static:

* variable files: Files that change on a regular basis. Some variable files
  are logs in ``/var``
* static files: Files that do not change on the system, like binaries.

Common Directories
^^^^^^^^^^^^^^^^^^

* ``/bin``:  Essential command binaries
* ``/boot``: static files required to boot
* ``/dev``:  devices and special files
* ``/etc``:  host-specific system configurations
* ``/home``: user home directories
* ``/lib``:  shared libraries and kernel modules
* ``/media`` and ``/mnt``: mount points for removable media
* ``/opt``:  optional add on files for software packages
* ``/proc``: kernel and processes information. (virtual file system)
* ``/root``: home directory for the root user
* ``/sbin``: essential system binaries
* ``/srv``:  web server data
* ``/tmp``:  temporary files
* ``/usr``:  secondary hierarchy for shareable, read-only data:

  * ``/usr/bin``: executable commands
  * ``/usr/include``: standard source header files
  * ``/usr/sbin``: non-vital system binaries
  * ``/usr/share/man``: manual pages
  * ``/usr/src``: source code, usually kernel source code.

* ``/var``:  variable data files

  * ``/var/cron``: crontab files
  * ``/var/log``: log files and directories
  * ``/var/spool``: application spool data

.. _filetypes:

Types of Files
^^^^^^^^^^^^^^

In Linux, everything is a file, including directories. They are all treated
the same way by standard Unix commands. When running ``ls`` or ``stat`` on a
file, it will output some information about the file::

    $ ls -l
    total 12
    drwxr-xr-x 1 wgiokas users  0 Feb 16 00:29 directory/
    prw-r--r-- 1 wgiokas users  0 Feb 16 00:29 fifo-pipe|
    -rw-r--r-- 2 wgiokas users 15 Feb 16 00:28 file
    -rw-r--r-- 2 wgiokas users 15 Feb 16 00:28 hardlink
    lrwxrwxrwx 1 wgiokas users  4 Feb 16 00:28 symlink -> file

As you can see on the left side there is a set of letters. Each of these has
a special meaning:

=== ======================= ===========================
sym Meaning:                Example
=== ======================= ===========================
b   block device            /dev/sda1
d   directory               /home
c   Character device        /dev/tty1
l   link to another file    ./symlink (see above)
p   named pipe (fifo)       ./fifo-pipe
s   Domain socket           ~/.tmux.socket
=== ======================= ===========================

Pathnames
^^^^^^^^^

There are two ways to specify file locations in Linux and UNIX: using
absoute or relative paths. 

Absolute paths always start at the root of the filesystem, ``/``. Shell
expansions such as ``~/`` or ``~user`` expand to ``/home/$USER``, so are
also considered to be absolute paths. 

Relative paths do not start with the root of the filesystem. These can
include paths like ``../bin`` or ``./foo``. They are relative to the current
PWD shell variable. 

* ``../`` means 'the directory above'
* ``./``  stands for 'the current directory'

If a user is currently in the directory ``/home/baz/foo`` and wants to get
to the directory ``/home/baz/bar`` there are multiple options:

* Use the full absolute file name: ``cd /home/baz/bar``
* Use relative paths: ``cd ../bar``
* Use shell expansions to create absolute paths: ``cd ~baz/bar``
* Go crazy and use relative and absolute paths:
  ``cd /home/john/../cindy/temp/../../baz/foo/../bar``

  I wish I wasn't lying when I said that you don't see things like that
  happen.

Navigating the File System
--------------------------

In the previous section I used the ``cd`` command. Here we will learn what
``cd`` and the rest of the coreutils family can do.

See ``info coreutils`` for full documentation on these commands.

cd
^^
Change to another working directory. Used for moving around the file system.

Syntax::

    cd /path/to/directory
    cd ./directory

mkdir
^^^^^
Make a directory. Normally mkdir can only make one directory in a hierarchy,
so calling ``mkdir ./newdir1/newdir2`` would return an error. The ``-p``
option causes mkdir to create the missing parent directories, in the example
the missing parent directory is ``./newdir1``.

Syntax::

    mkdir ./directory
    mkdir -p ./newdir1/newdir2

pwd
^^^
Print the working directory. This will print the current working directory
to standard output.

Syntax::

    pwd

stat
^^^^
Read file attributes. Shows information about a file or directory.

Syntax::

    stat /path/to/file
    stat ./file

touch
^^^^^
Touch a file. Creates an empty file or updates the timestamp of an already
existing file.

Syntax::

    touch /path/to/newfile
    touch ./file

cp
^^
Copy a file. Used to copy a file or group of files. To use on directories,
use the ``-r`` (recursive) option.

Syntax::

    cp /path/to/file /path/to/newfile
    cp -r /path/to/dir /path/to/newdir

mv
^^
Move a file or directory. Used to rename or move files to new locations.

Syntax::
    
    mv /path/to/file /path/to/newfile
    mv /path/to/dir /path/to/newdir

rm and rmdir
^^^^^^^^^^^^
These two commands remove files or directories. ``rm`` works on files, and
with the ``-r`` switch it also works recursively on directories. The
``rmdir`` command works only on empty directories. If the directory is not
empty, it will not delete it.

Syntax::

    rm /path/to/file
    rm -r /path/to/full/directory
    rmdir /path/to/empty/directory

File Permissions
----------------

The inodes in Linux hold the access rights for the files on your filesystem.
Certain actions cannot be taken if the current user does not have access
rights to the specified files. 

Let's say that the user ``root`` makes a file called ``foo`` in his home
directory (``/root``). By default, this file is going to have certain access
rights assigned to it, and will be owned by it's creator. Running ``ls -l``
on this file gives us something like this:: 

    -rw-r--r-- 1 root root 0 Feb 25 11:25 foo

Here we can see the access rights that this file has. In the first column is
the symbolic representation of the permissions. The third and fourth columns
tell us who the owning user and group are, while the fifth tells us the size
in bytes (It is currently zero). Next is the last modified date, and the
last part is the file name.

Octal and Symbolic Permissions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In Linux and UNIX there are a few ways to specify permissions, most common
being the octal and symbolic definitions. Again, we will take a look at
``root``'s file ``foo``::

    -rw-r--r-- 1 root root 0 Feb 25 11:25 foo

Earlier it was said that the first column is the symbolic representation of
the access rights. There are 10 characters in the first column, and starting
from the second character (the first ``r``) we can tell the permissions that
file has. Permissions are grouped in three bytes, one for read, write and
execute permissions. There are three groupings of these for the three
different levels of users, the owner, the group and others. Starting from
the second character::

    [ owner   rights ][ group   rights ][ other   rights ]
    [read][write][exe][read][write][exe][read][write][exe]

Here you can see how the different parts are separated. In the example above
with file ``foo``, you can see that it has ``rw-`` in the owner rights and
``r--`` in the group and other rights. This means that the owner of the file
is the only person who can write to this file, but other users and those in
the group ``root`` can read the file just fine.

The file permissions above are known as symbolic permissions, and are really
a human readable way of displaying them. To the computer, however, these are
just ones and zeroes. This allows us to use something called octal
permissions, where instead of ``rwx``, we would simply write ``7``. Each of
the three permissions -- read, write and execute -- are set to either one or
zero. If a file has read and write privelages, but no execute privelages,
this is the same as having ``rw-`` in symbolic notation. In binary, that
would simply be ``110``, which is equal to ``6`` (100 = 4, 010 = 2). Only read
permissions could be written as ``100``, or ``4``.

=== ===
sym bin
=== ===
r   100
w   010
x   001
=== ===

The standard utility to change the mode of a file is ``chmod``. There are a
number of ways to invoke this command, one is to change permissions on a
file. Let us say we have a new file, owned by my user called ``bar``::

    -rw-r--r-- 1 wgiokas users 0 Feb 25 11:25 bar

I then write some bytes to that file, making it a simple bash script. Only
problem is, I can't actually run this script because it does not have
execute permissions. I have two choices::

    chmod +x bar
    chmod 755 bar

Both of those commands do exactly the same thing. In the first, we add
execution (``x``) permissions to all of the blocks, setting the
permissions to ``-rwxr-xr-x``. In the second, we also set to those
permissions, but we expicitly change all of the permissions at once, making
the owner block ``rwx`` and the group and other blocks ``r-x``. This allows
all users to execute and read the script, but restricts writing to the
owner, ``wgiokas``.

The following is a simple conversion chart from the symbolic permission
notation to the octal permissions. For a more in depth explanation, see
``info file permissions``.

==================== =================
Symbolic Permissions Octal Permissions
==================== =================
rwx                   7
rw-                   6
r-x                   5
r--                   4
-wx                   3
-w-                   2
--x                   1
---                   0
==================== =================

Links
-----

Almost all standard Linux file systems allow for the creation of hard and
symbolic links (not all Windows file systems do this). Links are used for a
number of different reasons, and aid in file system organization. There are
two types of links, hard and symbolic. They are both very different

Hard Links
^^^^^^^^^^

Hard links have the same inode as the original file. If I create a file,
``foo``, it has a link count of one, meaning that one file is using this
inode. If I then create a hard link to that file::

    ln foo bar

then both ``foo`` and ``bar`` are now two files sharing the same inode. This
will bump up the link count to two, as there are now two files sharing this
inode. 

Hard linking is very useful, but it has some caveats: 

* Cannot link across block devices
* Cannot link directories, only files

If ``foo``, the original file, is deleted, then the link count will go back
to one, and ``bar`` will be the only link to the contents. These links, as
they share the same inode as their target, also share permissions.

Symbolic Links
^^^^^^^^^^^^^^

Sometimes called soft links, symbolic links point to a location in the
file system hierarchy. They get their own inode, and unlike hard links do
not increase the link count. :ref:`filetypes`

Using the file ``foo`` again, I will create a symbolic link::

    ln -s foo bar

Note the ``-s`` flag passed to ``ln``. This tells it to create a symbolic
link, as opposed to a hard link. These links, unlike hard links, can link to
directories and across block devices. However, if the file that a symbolic
link is linked to is moved or deleted, then the link becomes what is known
as a dangling symlink. Symbolic links only reference the location on the
disk, not the information within that location.

These links are also created with the permissions ``777``.
