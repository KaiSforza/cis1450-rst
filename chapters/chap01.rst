.. sectionauthor:: William Giokas <1007380@gmail.com>

Linux/Unix Overview
*******************

What is an Operating System?
============================

An operating system is software that talks to the hardware,
applications, and users. It allocates memory, storage and processing
time to applications and users. It also handles peripherals, such as USB,
network, serial, and parallel interfaces. 

There are two main parts to an operating system::

  * The kernel
  * System programs

The kernel talks to the system programs and the hardware, creating a way
for components to interact with hardware.

System programs interface with the user directly. These include text
editors, shells, word processors and web browsers.

UNIX
----

UNIX was one of the earliest server operating systems. It was developed in
1969 by a group of people employed by AT&T, including Ken Thompson,
Dennis Ritchie and Brian Kernighan. It was first written in pure assembly
language, but by 1973 it had been almost entirely converted to C. This
allowed it to be ported much more easily to new hardware.

The operating systems has since split into many different groups,
including the Berkley Software Distribution (BSD), HP/UX, Solaris, and Mac OS X.

Software based on the UNIX philosophy and principles was used heavily in
academic disciplines, particularly BSD. This led to adoption in many
commercial startups because it was familiar to the people starting them.

*Traditional Unix* is a term that may be used to describe operating
systems that have similarities to Version 7 Unix and UNIX System V.

Minix and Linux
---------------

Minix is a microkernel operating system written for
educational purposes by Andrew S. Tanenbaum. Released in 1987 under a
closed source license, it has since been made open source, released under
a BSD license in April of 2000. Originally meant solely for education, it
has since been repurposed to run on microcontrollers and other embedded
systems. Minix 1 was fully compatible with Version 7 Unix at the time of
its release.

Tanenbaum's OS was one of the inspirations for the Linux kernel. Linux,
originally written solely for the AT-386 line of processors by Linus
Torvalds, was released to the public on October 5th, 1991. It was thought
that the OS could never be used on other processors, but the contributors
quickly proved that assumption wrong. Linux has been ported to almost
every processor architecture known to man, a perk of it being the largest
open source computing project. 

Unix Design Features
--------------------

Designed as a multiuser, multitasking operating system, Unix was perfect
for a server environment in large companies. There were communications
frameworks built for system, and the Unix philosophy states that one
should 'write one program to do one job, and do it well.' This has carried
over to the Linux, which uses a similar philosophy. Pieces of a Unix or
Unix-like operating system can be easily replaced, upgraded, added, or
removed.

Layered Organization
--------------------

In Linux and Unix based operating systems, there are several layers of
'stuff' between the user and the actual hardware::

   ┌────────────────────────────────────────────────────────────┐
   │                            User                            │
   ├──────┼────┬──┼───┬─────────┼──────┬──────┼────────┬───┼────┤
   │ Compilers │ DBMS │ Word Processor │ Email program │ Shells │
   ├──────┼────┴──┼───┴─────────┼──────┴──────┼────────┴───┼────┤
   │                      UNIX/Linux Kernel                     │
   ├──────────────────────────────┼─────────────────────────────┤
   │                           Hardware                         │
   └────────────────────────────────────────────────────────────┘

The kernel manages interaction between the hardware and the programs, such
as scheduling tasks, queuing storage, assigning CPU priorities.

The shell manages interactions between the user and programs.
Most commands typed into a shell are separate from the kernel, but call
routines in the kernel to get resources for the programs.

Free and Open Source Software
=============================

Richard Stallman
----------------

Richard Stallman started the GNU (Gnu's Not UNIX) project in 1983. His
beliefs were that software should be accessible, and that this was crucial
for effective development. In 1985, the Free Software Foundation was
founded by Stallman, and the GNU General Public License (GPL) was released
in 1989. The GNU team wrote most of the userspace tools that were present
in the early 90's, such as a text editor, compiler, libraries, a shell and
a windowing system. There were a few components missing from this
operating system, though, including a kernel, device drivers, and daemons.
The GNU project still works on a kernel, the HURD, but after the
widespread adoption of Linux, its development has been slow.

Linus Torvalds
--------------

Linus Torvalds, the benevolent dictator of the Linux kernel, was a
graduate student when Stallman and the GNU project was writing its
software. Linus, fed up with licensing of MINIX and the cost of UNIX,
began to work on his own operating system. Originally called Freax, the
kernel developed by Linus filled the gap that the GNU project needed.
Linux was licensed under the GNU GPL, and compiled with the GNU C Compiler.
This reliance on GNU tools has caused some controversy over the years as
to the naming of the Linux based operating systems available.

The Cathedral and the Bazaar
----------------------------

In 1999, Eric Raymond published a book comparing the open source and the
closed source development models. At the time, Linux had been seeing
adoption by hobbyists, but many companies didn't think that the open
source development style was a viable alternative, even given the rapid
and stable development of the Linux kernel and various other projects.

The **cathedral** model, used by corporations and certain open source
groups, is defined by having a core group of developers that works on the
code in, what Raymond calls, a cathedral. Only these few developers have
access to the source code before a stable version of the software is
released. Some projects that were given as examples were the GNU Emacs
text editor and the GNU C Compiler.

In the **bazaar** model, code is constantly released, usually over the
internet, to be reviewed by as many people as possible before a 'stable'
release is made. The Linux kernel and Raymond's own experiment in this
model, Fetchmail, are given as examples.

Raymond's central thesis in the book was that "given enough eyeballs, all
bugs are shallow," which is termed Linus' Law in his book. The more
available for public scrutiny and testing the code is, the faster and
easier bugs can be fixed. If the code is not released, then an inordinate
amount of time must be spend finding and fixing bugs, since only a small
group of people are able to test the code. 

The publishing of this book pushed Netscape to release their slowly
dwindling browser using this open source development model and becoming
the Mozilla group.

Features and benefits of Linux
------------------------------

* POSIX Standards compliant
* Programs interact through system calls
* Free and commercial applications available
* Support for a wide range of peripheral devices
* Massively multi-platform
* Written in highly portable C
* Designed to be multi-user
* Protected multi-tasking
* Many secure file systems available in the kernel itself
* Interprocess Communication (IPC)
* Strong virtualization support built into the kernel

User Interfaces in Linux
========================

The Shell
---------

One of the main points of interaction between the user and computer in
Linux is the shell. Shells are a high level programming language. The
shell itself is simply a command interpreter, and can be used to execute
other programs. Shells make extensive use of wildcard characters (\*) and
most allow for command, filename and variable completion systems.
Redirection using pipes (|) and greater-than and less-than operators (>
and <) are common for working with programs and files. They also have
quite powerful job control systems built in.

There are a plethora of shells to choose from:

* Bourne Again Shell (bash), is an enhanced version of the original Bourne
  Shell on Unix systems.
* TC Shell (tcsh) is an enhanced version of the original C Shell. It it
  BSD Unix compatible.
* The Z Shell (zsh) incorporates many features from bash, tcsh, ksh, and
  more.
* KornShell (ksh) was developed by David Korn and is similar to the C
  shell.
* Debian Almquist Shell (dash) was written to replace bash with a simpler,
  faster alternative at the expense of features.

Graphical Interfaces
--------------------

X Window System and Wayland
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Currently the standard graphical interface used on Linux and UNIX systems,
it was first released in 1984 from MIT as a part of Project Athena. Their
goal was to create a platform-independent graphics system for a large
network of varied computers and devices. 

In 1988 the MIT X Consortium was formed in order to keep the development
of X headed in a neutral direction for use by multiple companies. 

X11 follows a simple client-server model. Even on a non-networked machine,
network protocols are used to communicate to the display. This allows
multiple machines to use the same hardware for computation, but have
independent remote hardware. This is useful when you are working on
projects that require certain hardware, but it is not feasible to give a
personal copy to all of the developers or users. 

The Wayland compositor is a modern attempt to replace the aging X server
with a cleaner, faster implementation. Application support for Wayland is
still lacking, though.

Window Managers
^^^^^^^^^^^^^^^

There are two real parts to a fully functioning desktop: The window
manager and the applications that are managed by the window manager. Many
Linux desktops today use one of four major display managers:

* Metacity: This was the window manager behind Gnome 2. 
* Mutter: As of Gnome 3, the default window manager was changed to
  Mutter.
* KWin: The default window manager for KDE.
* Compiz: The window manager for Ubuntu's Unity desktop environment.

Window managers can be used on their own, hand selecting applications to
do the things you need, or they can be found bundled in a Desktop
Environment (DE). A DE is a software compilation that can give you a fully
functioning desktop computer with little or no help from outside
applications. Many users choose to go with a DE, as it eases use most of
the time. There are many window managers that are meant to be used on
their own, such as DWM, wmii, i3, xmonad and openbox. Many of these are
lightweight solutions for users with underpowered machines, or those that
do not want any frills on their desktop.

Desktop Environments
^^^^^^^^^^^^^^^^^^^^

A Desktop Environment (DE) is a compilation of software usually consisting of a
window manager, desktop manager (also known as a login manager), and
applications that are used with the desktop environment such as email
clients, text editors, and icon sets. Some common Desktop Environments
would be Gnome, KDE, LXDE, and XFCE. Many distributions come with one of
these pre-installed, though as with any Linux applications, installing a
different one is as easy as using your package manager. A user can have
multiple DEs on his workstation, which he or she can choose to run at login
time.
