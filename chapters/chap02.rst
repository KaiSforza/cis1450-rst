.. sectionauthor:: William Giokas <1007380@gmail.com>

Getting Started with Linux
**************************

Accessing the COD Linux Server
==============================

Linux was built from the beginning as a multi-user system. This means that
it supports having multiple users logged in at the same time on one system.
Even simple workstations have multiple users to keep data secure.

The system administrator creates accounts on a machine, and can give access
to these accounts to its users. Normally, there are at least two accounts on
any give Linux machine: a root account with full access to everything on the
system, and a user account for every day computing. Users have many
different ways to sign in, directly on the machine through a tty, or using a
pseudo-tty from a remote location. The latter can be done in two ways.

Telnet
------

Telnet is a network protocol that provides two-way interactive text
communication. It does so using a virtual terminal connection. Telnet is an
unencrypted text transfer protocol, making it very vulnerable to attacks. It
has largely been phased out in favor of ssh.

Secure Shell
------------

Secure Shell, or SSH, is a secure cryptographic network protocol for data
transmission. It is mainly used in UNIX and Linux systems to administer
headless servers or other remote machines. When connected to another
machine, it allows the user to execute commands on that machine. The COD
Linux server uses this protocol.

Connecting to the Server
------------------------

You should have been given a username and password by your system
administrator, but in this demonstration I am going to use two machines: My
local machine, ``foo``, and a remote machine, ``baz``. I will be using the same
username on both machines, ``bar``.

To connect over ssh, first open a terminal window. I get a prompt that looks
like so::

    bar @ foo:~ $

This prompt tells me that I am logged in as the user ``bar`` at the machine
``foo``, and that I am in my home directory (``~``). On your local machine,
run the command ``info ssh`` to get more information on this command. We
will be using the ``-p`` flag to specify a port when connecting. 

Now my administrator has taken the time to add my account, and I know my
username and password on the remote machine, so to log in remotely using ssh
I would type (the port my administrator uses is 31950, specified by the
``-p`` flag)::

    bar @ foo:~ $ ssh -p 31950 bar@baz

If this is the first time you have connected to this server, you will see a
warning::

    The authenticity of host 'baz (192.168.43.217)' can't be established.
    ECDSA key fingerprint is 3b:36:84:0c:b3:8e:41:ea:ab:c6:f7:d3:bc:c2:a8:a8.
    Are you sure you want to continue connecting (yes/no)?

Type out the word ``yes`` at the prompt if you trust the host. 

.. note::
  The ECDSA fingerprint and the ip address will not be the same when you try to
  connect to a system.

Then a prompt for your password will appear::

    bar@baz's password: 

Enter the password your administrator gave you.

.. note::
  No text or asterisks will show up. This is for security reasons. 

Once you are connected to the server, you will see a prompt like this::

    bar @ baz:~ $

As you can see, we are now logged on as the user ``bar`` at the machine
``baz``, and we are in our home directory. 

Unlike in DOS, things in UNIX and Linux are case sensitive. Capitalization
matters. Some administrators add a message of the day (MOTD) to their servers
for users to see when they log on giving information about the server. This
is stored in ``/etc/motd``.

When you have done all your work on the remote server and you want to log
out, then you can type ``exit`` at the remote prompt to close your session.
Another way to do the same thing is to type ctrl+d. This sends the ``EOF``
signal to the server, telling it that you are done.
