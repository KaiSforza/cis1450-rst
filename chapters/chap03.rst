.. sectionauthor:: William Giokas <1007380@gmail.com>

Getting Help
************

Linux and UNIX generally have very extensive help built into the software
packages that are distributed. There are a few different kinds of help texts
commonly used: info and manual (man) pages.

Shell Commands
==============

Each shell command has its own arguments and parameters. Generally, only a
few should be memorized, as they can easily be found out using manual pages
and/or info pages. 

There are 4 types of commands that a shell can interpret

* builtin: Included in the shell itself
* executable: A binary file usually included in /bin, /usr/bin or their sbin
  counterparts. Found by the shell using the ``PATH`` variable. This also
  includes shell scripts in the user's ``PATH``.
* shell function: A function written specifically for the shell in the
  language of the shell (bash, zsh, etc.). These are not to be confused with
  scripts, which are a type of executable. 
* alias: A user defined command that calls other commands. Usually defined
  in the shell configuration file.

Commands to Get Help
====================

``help``
--------

``help`` is the builtin shell help function in bash. It can only display
information on builtin shell commands, such as ``help`` and ``alias``. More
extensive help can usually be found in manual and info pages.

Example::
    
    help alias
    help echo

Manual Pages
------------

Manual pages are the original 
