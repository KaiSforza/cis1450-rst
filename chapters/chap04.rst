.. sectionauthor:: William Giokas <1007380@gmail.com>

Utilities
*********

Interacting With the Shell
==========================

Shells are generally more powerfull, better supported, and more complete
than their graphical counterparts. There is a simple process that takes
place when a user enters a shell command::

    User enters  <- Shell displays   <-- Utilty informs shell
      command       prompt               that it completed
         |                                 ^
         V                                 |
    Shell starts    Shell tells child    Utility 
    child process ->to execute utility ->program runs
                    code
    
This repeats until the shell is told to exit or the computer dies.

Special Characters
------------------

These characters have a special meaning to the shell:

``& ; | * ? ' " ` [ ] ( ) $ < > { } # \ / ! ~``

These characters should not be used in filenames and must be escaped or
quoted to prevent the shell from interpreting them (This does not apply to
the ``$`` character).

Examples::

    foo @ bar:~ $ echo this is a prompt >
    bash: syntax error near unexpected token`newline'
    foo @ bar:~ $ echo this is a prompt \>
    this is a prompt >
    foo @ bar:~ $ echo 'this is a prompt >'
    this is a prompt >

Redirection
===========

Some of these special characters affect where the output of certain commands
will go. The ``< > |`` characters cause redirection of a command's output.
The ``>`` character, passed at the end of a command, will cause the output
to be redirected to a file. Example::

    foo @ bar:~ $ echo "This is a test of redirection." > redirect.txt
    foo @ bar:~ $ cat redirect.txt
    This is a test of redirection.

Now, if the file ``redirect.txt`` had already existed and I ran that
command, it would have been overwritten. To simply append to a file, use two
``>`` characters. Example::

    foo @ bar:~ $ echo "appending to a file" >> redirect.txt
    foo @ bar:~ $ cat redirect.txt
    This is a test of redirection.
    appending to a file

Note that the file has an extra line, and retained the original. Next is the
``|``, or the pipe. This pass the output of a command to another command as
standard input, so running::

    foo @ bar:~ $ echo "test 1 2 3" | grep -o 'test'
    test

(The ``-o`` flag for grep causes it to only print the matching portion of
the line)


Basic Utilities
===============

========  =========================
ls        lists the names of files
hostname  displays the local system name
cat       concatenate and print files
rm        delete a file or directory
more      display long files one screen at a time
less      display long files one screen at a time
cp        copies files
mv        move or rename a file
grep      search files
head      display the beginning of a file
tail      display the end of a file
sort      print sorted file to standard output
uniq      print file to stdout, skipping duplicate lines
diff      compare two files and print the difference to stdout
file      identify the contents of a given file
echo      print text to stdout
date      print date to stdout
script    record session (``exit`` stops recording)
gzip      compress a file using Lempel-Ziv encoding
bzip2     compress a file using block text compression
tar       store files in a tape archive
who       lists users currently logged on
finger    user information lookup
w         lists users logged on and what they are doing
write     sends message to logged on user
mesg      deny or accept messages from other users
========  =========================

Core Utilities
--------------

Also known as coreutils, these utilities make up the base of any Linux
operating system. See ``info coreutils`` for a full explanation of what
coreutils contains.

Archiving
---------

There are many different compression algorithms you can use when compressing
files. I will go into detail about the gzip, bzip2 and xz utilities.

gzip
^^^^
The standard compression algorithm for Linux, it has one of the faster
decompression and compression times of the three utilities. You can use
``gzip`` to archive files, ``gunzip`` to unarchive a file with the extension
``.gz``, and use ``zcat`` on a file to print its contents to standard
output. It can also be used by the ``tar`` command to create a gzipped
tarball by passing the -z option to ``tar``.

bzip2
^^^^^
Another fast compression and decompression algorithm, bzip2 uses the
Burrows-Wheeler block sorting text compression algorithm. It is also
generally better at compressing files than the ``gzip`` command. The
arguments and extra commands are similar to that of ``gzip``: ``bunzip`` to
decompress a file and ``bzcat`` to print a file to stdout.

bzip2 can be used directly by tar by passing the ``-j`` option to tar.

xz
^^
A general purpose data compression tool that compresses files to either its
native ``.xz`` format or the legacy ``.lzma`` format. The compression
algorithms used by xz tend to make it one of the most powerful compressors
in common usage, but also one of the slower, more memory hungry options. On
modern systems this is of little consequence, though. xz also has options
similar to gzip, such as ``unxz`` and ``xzcat``.

xz can be used directly by tar by passing the ``-J`` option to tar.

