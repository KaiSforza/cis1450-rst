.. CIS 1450: Intro to Linux/Unix documentation master file, created by
   sphinx-quickstart on Fri Feb 15 14:52:59 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. sectionauthor:: William Giokas <1007380@gmail.com>

CIS 1450: Intro to Linux/Unix
#############################

Welcome to the texinfo version of the Introduction to Linux/Unix class
offered at the College of Dupage. Please press H for detailed help on using
this texinfo document. Press h for a list of keybindings in the 'info'
program.

If there is something missing or wrong in this document, or if you want to
ontribute information to this document, please feel free to send  me an
email at 1007380@gmail.com. This texinfo document was generated using the
`Sphinx documentation generator`_. The source for this texinfo document can
be found on `bitbucket`_.

This documentation is based off of the power points and homework given by
Professor England. I make no guarantees that the information in here is
correct or unbiased, or that it will be of use to anyone.

This documentation is licensed under the MIT/X Consortium License. Please
see :ref:`license` for more information.

.. toctree::
   :maxdepth: 2
   :glob:
   :numbered:

   chapters/chap*

   extra/index
   LICENSE

.. _Sphinx documentation generator: http://sphinx-doc.org/index.html
.. bitbucket link:
.. _bitbucket: https://bitbucket.org/KaiSforza/cis1450-rst
