CIS 1450 reStructureText repository
===================================

This is a repository containing reST versions of the slides and class
notes, as well as extra things that are interesting to some people.

Style:
-----
Try to keep your lines less than 76 characters long.

See `this document`_ for more information on the syntax used.


.. _this document: http://sphinx-doc.org/rest.html#source-code
