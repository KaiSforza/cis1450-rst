.. CIS 1450: Intro to Linux/Unix documentation master file, created by
   sphinx-quickstart on Fri Feb 15 14:52:59 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. sectionauthor:: William Giokas <1007380@gmail.com>

Extra Information
#################

This section contains extra information on topics that I find interesting.

Contents:

.. toctree::
   :maxdepth: 1
   :glob:
   :numbered:

   *

.. _Sphinx documentation generator: http://sphinx-doc.org/index.html
.. bitbucket link:
.. _bitbucket: https://bitbucket.org/KaiSforza/cis1450-rst
