Linux Distributions
*******************

Distributions are, many times, distinguished by their package managers.

For more information on package managers:

:ref:`package-managers`.

Slackware Based
===============

Slackware Linux
---------------

SUSE Linux
----------

RPM Based
=========

Originally an acronym for ``Red Hat Package Manager``, it now simply stands
for ``RPM Package Manager``, a recursive acronym. See the `RPM wiki article`_
for more information. 

Red Hat Enterprise Linux
------------------------

Originally just Red Hat Linux, it was one of the earliest commercial Linux
distributions. RHL was discontinued in 2004, superseded by Red Hat
Enterprise Linux. RHEL uses the RPM package management system. It is a
commercial distribution, and so the company Red Hat stands behind these
products, offering paid support for their software. 

Home Page: `redhat.com`_

Fedora Linux
------------

Fedora is a community supported distribution that Red Hat releases. It
usually has the latest packages and is used for testing integration for use
in RHEL. Support for Fedora is generally short lived, and there are releases
every 6 months. The Fedora Project contributes a considerable chunk of code
to the Linux kernel.

Home Page: `fedoraproject.org`_

Debian Based
============

Debian
------

Debian is not a Linux specific distribution. There is Debian GNU/Linux, but
there are also officially supported version for GNU/kFreeBSD and GNU/HURD.
It uses the ``dpkg`` package manager, usually seen from its ``apt`` front
end. This is the basis for a large portion of the Linux distributions that
are currently available, including Ubnutu.

Home Page: `debian.org`_

Ubuntu Linux
------------

A free commercial Linux distribution based on Debian GNU/Linux supported by
Canonical. Like most Debian distributions, it uses heavily patched packages
in an effort to give the user the best experience possible. This means that
they are generally quite a ways behind the leading edge of development.
Ubuntu releases every 6 months, and they release a long term support (LTS)
release every 2 years.

Home Page: `ubuntu.com`_

Independent
===========

Arch Linux
----------

A Linux distribution  based on the pacman library based package manager.
Started by Judd Vinett in 2001, it has since grown to be one of the more
respected independent distributions. Most of the administrations tools do
not have any graphical interface and almost everything is expected to be
configured with a text editor. The distribution prides itself on running
mostly vanilla upstream packages, usually releasing new versions of packages
to the testing repository within an hour of the upstream release. Many of
the developers of Arch also work on these upstream projects.

Another distinguishing feature of Arch is its modularity. Using a ports-like
build system known as the Arch Build System (ABS), users can easily
customize and compile their own packages custom tailored to their systems.
While there may not be tens of thousands of packages in the official
repositories, the Arch User Repository (AUR) offers unsupported packages
that users can compile themselves in similar fashion to the ABS. Many
development packages that build directly from git repositories are in the
AUR, allowing users to easily keep up to date with the bleeding edge of
development.

Arch historically used the System V initialization system, following the BSD
style of having a few ``rc`` configuration files. Recently, Arch has
switched to the systemd initialization system, sparking controversy from its
users. Many found the old style ``rc`` scripts easy to use and thought that
it was a central tenant of the distribution. The developers that made the
decision put forth strong technical arguments, and most of the community has
accepted systemd gracefully.

Home page: `archlinux.org`_

Gentoo
------







.. _RPM wiki article: http://en.wikipedia.org/wiki/RPM_Package_Manager
.. _redhat.com: http://www.redhat.com/
.. _fedoraproject.org: http://www.fedoraproject.org/
.. _debian.org: http://www.debian.org/
.. _ubuntu.com: http://www.ubuntu.com
.. _archlinux.org: https://www.archlinux.org/
